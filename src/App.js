// noinspection JSCheckFunctionSignatures

import React, { Suspense, lazy } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { AuthConsumer } from "./components/auth/AuthProvider";

const Home = lazy(() => import('./components/containers/Home'));
const Room = lazy(() => import('./components/containers/Room'))

function App() {
  return (
    <AuthConsumer>
      {(context) => (
        <BrowserRouter>
          <Suspense fallback={<div>Loading...</div>}>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route
              path="/room/:roomID"
              component={() => <Room user={context.user} />}
            />
          </Switch>
          </Suspense>
        </BrowserRouter>
      )}
    </AuthConsumer>
  );
}

export default App;
